---
layout: post
title: "[청약/분양] 경기도 의왕 센트라인 데시앙 분석 (위치, 인구수, 수요와 공급, 매매/전세지수, 가격비교, 학군, 상권까지)"
toc: true
---

 오전나구역 주택재개발정비사업으로 추진되었으며
 총 733세대 반도 조합원 분양 이후, 일반분양(532세대)에 대한 분양정보임
 ​
 모집공고일 : 2023-10-30 /

 특별공급(특공) : 2023-11-09 /

 1순위 : 2023-11-10 /

 2순위 : 2023-11-03
 ​​
 자리 : 경기도 의왕시 오전동 32-4번지 일원
 경기도 의왕시 오전동 32-74
 1차 해석 : 인구수 / 세대수(👎)
 경기도 의왕시의 인구수는 약 16만명 전후임
 세대수는 증가추세였으나 2021년이후로 감소추세!
 (초보투자자에게 20만 공상 / 세대수가 증가하는 자못 추천)
 출처 : 부동산지인
 2차 해석 : 수요와 공급(👎)
 ​
 경기도 의왕시는 2017년 이후로 미분양이 없다가

 2023년에 중수 이상의 미분양이 발생함
 악성미분양인 준공후 미분양은 없으나
 2025-2026년에 수요보다 많은 입주가 예상됨
 (가격 상승 작정 : 준공 추후 미분양이 없거나 감소 전환)
 출처 : 부동산지인
 이즈막 3년간 의왕시는 안양시에서만 귀퉁이 인구유입이 있고
 수원시, 화성시 등으로 많은 인구 유출이 발생하고 있음
 출처 : 호갱노노
 3차 검토 : 매매/전세지수(👍)
 ​
 의왕시는 2022년 상승 이후 지수가 동반 하락함
 전세지수보다 매매지수가 높거나 비슷한 수치였으나
 2023년 3월부터 매매지수가 전세지수보다 낮음
 단, 2023년 9월에 전세지수가 급증하였으나 10월에 다시 매매지수와 비슷한 수치로 떨어짐
 ​
 (매수 짬 : 매매 지수가 전세지수를 뚫고 내려가는 촌분 /

 매매지수가 요즈음 몇년간 하락한 떵 + 전세지수가 횡보하다가 상승 전환한 지역)
 출처 : 아실
 4차 해석 : 가격비교(👎)
 ​​
 의왕 센트라인 데시앙 인근*에 위치한 아파트(도시형, 주상복합 제외) 중

 입주 예정일 목표물 5년 즉변 300세대 이상, 1,000세대 미만인 4곳의 가격확인
 *반경 2.0km 이내
 지도출처 : 네이버부동산
 현재 인근 아파트와 가격으로만 비교 분석한 결과,

 의왕 센트라인 데시앙의 분양가격은 유사평형 값 평균과 비슷하거나 비쌈
 전용 37㎡의 분양 가격은 2022년 아파트의 동일 평형과 비슷한 가격임
 전용 59, 84㎡의 예 유사 평형의 가액 평균보다 5천만원~1억원 비쌈
 전용 59, 84㎡의 분양가격은 평촌두산위브더프라임의 분양권가격에 비해 비쌈
 의왕 센트라인 데시앙의 전용 98㎡의 호소 인근에 유사평형이 없으나,

 2024년 입주 예정인 평촌어바인 퍼스트더샵 103㎡ 분양권보다 비싼 가격임
 (타 아파트의 정경 네이버 부동산 최저 호가 기준으로 비교함)
 5차 검토 : 학군(👍)
 ​
 의왕시의 학업성취도는 경기도 평균보다 높음
 의왕 센트라인 데시앙 다만 인근에 호성중, 모락중은 학업성취도가 높은편임
 학원가는 근처 아파트 단지에 있고
 학원가 밀집지역은 평촌, 산본으로 자동차로 약 10분 거리임
 [의왕센트라인데시앙](https://note-grape.com/life/post-00104.html) 출처 : 아실 / 호갱노노
 6차 검토 : 상권(👍)
 ​
 의왕 센트라인 데시앙 겨우 인근에 병원, 약국, 편의점 등 기본적인 편의시설이 갖추어져 있음
 안양 호계동 주변(자동차로 5분거리)에 다양한 편의시설이 밀집되어 있음
 대형마트(홈플러스, 이마트)는 자동차로 약 5분 거리에 있으며
 1호선 군포역, 금정역까지 자동차로 10분 이내에 도착 가능함
 지도출처 : 카카오맵
 <"의왕 센트라인 데시앙" 해석 요약>
 ​
 1차 검토 : 인구수 / 세대수(👎)
 2차 검토 : 수요와 공급(👎)
 3차 검토 : 매매/전세지수(👍)
 4차 해석 : 가격비교(👎)
 5차 검토 : 학군(👍)
 ​6차 해석 : 상권(👍)
 ​+
 수도권 비투기과열지구 및 비청약과열지역(분양가상한제 미적용)
 → 전매제한기간 1년(특별공급/일반공급)
 ​(계약금 10% / 중도금 60% / 잔금 30%)
 ​​
 ↓↓↓↓↓ 기저 입주자모집공고문을 참고하세요 ↓↓↓↓↓
 전야 원하는 청약에 당첨되시길 기원합니다 'ㅁ'

 검토 방법은 "나는 1,000만원으로 아파트 산다
 -시크릿브라더-"을 참고했습니다​​​
 <청약결과 & 경쟁률>
 여러분의 공감은 매양 힘이 됩니다 꾸욱 눌러주세요
 ​
