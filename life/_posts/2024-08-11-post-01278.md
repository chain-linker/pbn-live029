---
layout: post
title: "소름끼치는 영화 추천 모음 1탄"
toc: true
---


 안녕하세요. 개뿌립니다.

 

 오늘은 왜인지 소름 끼치는 영화들로 준비를 해왔습니다.
 어딘가 모르게 섬뜩하고, 진상 대놓고 소름이 돋는 영화들로 쫄보 시청자 분들은 하나 시청을 주의하시기 바라며 따뜻한 담요 한동아리 준비해 오소소 돋을 닭살을 가라앉힐 준비를 해놓으시길 바랍니다.
 

 

 #영화추천 #소름영화

## 1. 비바리움
 ★★★★
  넷플릭스  /  왓챠플레이  /  웨이브  /  티빙TVING  /  네이버시리즈  /  U+모바일tv

 

 #미스터리, #공포, #SF

 첫 번째로 소개해드릴 영화는 '비바리움 (Vivarium, 2020)'입니다.

 이사 갈 집을 구하고 있는 '톰 (제시 아이젠버그)'과 '젬마 (이모전 푸츠)'는 어느 부동산 중개인으로부터 '욘더'라는 마을의 9호 집을 소개받습니다.

 그런데 집을 구경하던 와중에 사라진 부동산 중개인.
 무언가 수상함을 느낀 '톰'과 '젬마'는 욘더 마을을 빠져나가려고 반면 뫼비우스 띠처럼 모든 길은 9호 집으로 연결되어 있었고, 두 사람은 9호집을 벗어날 행복 없었습니다.

 두 사해동포 앞에 놓인 의문의 상자.
 댁네 안에는 신생아가 있었고 상자 옆면에는
 "아이를 기르면 너는 풀려날 것이다."라는 글이 쓰여있었습니다.

 '비바리움'은 관찰이나 연구를 목적으로 동물과 식물을 가두어 사육하는 공간을 뜻합니다.
 

 어김없이 욘더라는 마을의 9호 집이라는 집이 인류가 꾸린 [주소모음](https://unwieldypocket.com/life/post-00113.html) 가정, 인생을 축소한 것만 같습니다.

 물론, 삶의 값진 의미와 행복이라는 기름기를 쫙 빼낸 퍼석한 불편함만 담고 있지만 말입니다.
 

 시고로 극단적인 세계관이라 보는 그냥 소름 끼치는 기분이 들어 무서운 불귀 단독 궁핍히 공포감을 불러일으키기도 했습니다.

## 2. 오펀: 천사의 비밀
 ★★★★★
  U+모바일tv

 

 #미스터리, #공포, #스릴러

 두 번째로 소개해드릴 영화는 '오펀: 천사의 기밀 (Orphan, 2009)'입니다.

 '케이트 (베라 파미가)'와 '존 (피터 사스가드)'는 초삭 입양을 위해 고아원에 방문합니다.
 그곳에서 두 사람은 꼭 부러진 유영 '애스터 (이사벨 펄먼)'을 만나게 됩니다.

 '이사벨'을 입양하기로 결심한 부부는 큰 아들과 막내딸까지 다섯 식구의 행복한 삶을 꿈꿨지만, 이들에겐 예상치 못한 일이 발생합니다.
 현 예상치 못한 일이 너무나도 예상치 못했던 일이라 소름이 끼쳤던 영화입니다.
 영화가 끝나고 나서도 한참 여운이 가시지 않았던 영화로 자전 영화를 앞 접해보는 이들이시라면 시청해 보실 것을 추천드립니다.

## 3. 겟 아웃
 ★★★★☆
  넷플릭스  /  왓챠플레이  /  웨이브  /  쿠팡플레이  /  U+모바일tv

 

 #미스터리, #공포, #스릴러

  세 번째로 소개해드릴 영화는 '겟 아웃 (Get Out, 2017)'입니다.

 '로즈 (앨리슨 윌리암스)'는 주말을 맞아 부모님께 남자친구 '크리스 (다니엘 칼루야)'를 뇌 소개하는 자리를 마련합니다.

 여자친구 가족들의 친절한 맞음이 있었지만 어쩐지 기분이 이상했던 '크리스'는 상상치도 못한 진실을 마주하게 됩니다.

 '겟 아웃'은 영화가 전체적으로 소름 끼친다기보다는 드문드문 소름 끼치는 귀토 장면들이 뇌리에 몸길이 박히는 느낌의 영화입니다.
 아무아무 의미의 소름이든 소름을 끼치게 하는 영화는 그자체로도 대단한 힘을 가진 영화임이 분명합니다.
 마음의 동요를 줄 행복 있었던 만치 몰입감을 가지고 있음을 말하는 것일 테니까요.
 이전에 포스팅했던 소름 영화 모음 링크를 남기며 마무리하겠습니다.
 

 https://gaebbul.tistory.com/166
 https://gaebbul.tistory.com/144
 https://gaebbul.tistory.com/115
 https://gaebbul.tistory.com/103
 https://gaebbul.tistory.com/101
 https://gaebbul.tistory.com/24
 https://gaebbul.tistory.com/195
 https://gaebbul.tistory.com/125
 https://gaebbul.tistory.com/208
 https://gaebbul.tistory.com/227
 https://gaebbul.tistory.com/163
 https://gaebbul.tistory.com/162
 https://gaebbul.tistory.com/160
 

 https://gaebbul.tistory.com/153
 https://gaebbul.tistory.com/90
 

 

 https://gaebbul.tistory.com/205
 https://gaebbul.tistory.com/226
 https://gaebbul.tistory.com/120
 https://gaebbul.tistory.com/203
 https://gaebbul.tistory.com/197
 https://gaebbul.tistory.com/88
 https://gaebbul.tistory.com/11
 https://gaebbul.tistory.com/194
 https://gaebbul.tistory.com/189
 https://gaebbul.tistory.com/181
 https://gaebbul.tistory.com/180
 https://gaebbul.tistory.com/183
 

 https://gaebbul.tistory.com/109
 https://gaebbul.tistory.com/192
 https://gaebbul.tistory.com/3
 

 이윽고 5월이 다가왔습니다.
 5월 한달간도 바지런히 달려 볼 테니, 볼만한 영화들을 찾고 싶으실 계제 들러주시면 감사하겠습니다.
 

 더욱더욱 다양한 영화가 궁금하시다면 블로그 내 최상단에 있는 '돋보기 버튼 (검색하기)'를 이용해 주세요.
 

 

 '개뿌립니다'에서는
 다양하고 재미있는 영화를 소개해드립니다.
 아래 구독하기도 상당히 해주세요♥
 ↓↓↓

 ↑↑↑
 구독하기 무지무지 해주세요.
 

 

 

 아래 태그 버튼을 누르시면, 연관 영화 추천 리스트를 확인할 핵 있습니다.
